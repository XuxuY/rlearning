import numpy as np

from src import Env


def road_to_right(env: Env) -> np.ndarray:
    pi = np.zeros((env.S.shape[0], env.A.shape[0]))
    pi[:, 1] = 1
    return pi
