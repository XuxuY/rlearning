import numpy as np

from src import Env


def road_to_left(env: Env) -> np.ndarray:
    pi = np.zeros((env.S.shape[0], env.A.shape[0]))
    pi[:, 0] = 1
    return pi
