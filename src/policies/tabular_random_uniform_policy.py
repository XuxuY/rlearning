import numpy as np

from src import Env


def tabular_random_uniform_policy(env: Env) -> np.ndarray:
    assert env.A.shape[0] > 0
    return np.ones((env.S.shape[0], env.A.shape[0])) / env.A.shape[0]
