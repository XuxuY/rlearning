from typing import Tuple

import numpy as np

from src import Env


def value_iteration(
    env: Env,
    P: np.ndarray,
    T: np.ndarray,
    Pi: np.ndarray,
    gamma: float = 0.99,
    theta: float = 1e-5,
) -> Tuple[np.ndarray, np.ndarray]:
    V = np.random.random((env.S.shape[0],))
    V[T] = 0.0
    delta = float("inf")

    while delta > theta:
        delta = 0
        for s in env.S:
            old_v = V[s]

            formula = lambda a: np.sum(  # noqa
                P[s, a, s_p, 0] * (P[s, a, s_p, 1] + gamma * V[s_p]) for s_p in env.S
            )
            best_action = max(env.A, key=formula)
            best_action_score = formula(best_action)

            V[s] = best_action_score
            delta = np.max(np.array([delta, abs(old_v - V[s])]))

    for s in env.S:
        formula = lambda a: np.sum(  # noqa
            Pi[s, a] * P[s, a, s_p, 0] * (P[s, a, s_p, 1] + gamma * V[s_p])
            for s_p in env.S
        )
        best_action = max(env.A, key=formula)

        Pi[s] = 0.0
        Pi[s, best_action] = 1.0
    return Pi, V
