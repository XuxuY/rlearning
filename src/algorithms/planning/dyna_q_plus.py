from math import sqrt
from typing import Tuple

import numpy as np

from src import Env


def dyna_q_plus(
    env: Env,
    episodes: int = 1000,
    n: int = 100,
    gamma: float = 0.99,
    alpha: float = 0.01,
    epsilon: float = 0.75,
    k: float = 0.9999,
) -> Tuple[np.ndarray, np.ndarray]:
    Q = np.random.random((env.S.shape[0], env.A.shape[0]))
    reward_model = np.random.random((env.S.shape[0], env.A.shape[0]))
    s_p_model = np.zeros((env.S.shape[0], env.A.shape[0]), dtype="uint8")
    Pi = np.random.random((env.S.shape[0], env.A.shape[0]))
    tau = np.zeros((env.S.shape[0], env.A.shape[0]), dtype=np.uint64)

    for s in env.S:
        if env.is_terminal(s):
            Q[s, :] = 0.0
            Pi[s, :] = 0.0

    s = env.reset()

    for _ in range(episodes):
        rdm = np.random.random()
        a = np.random.choice(env.A) if rdm < epsilon else np.argmax(Q[s, :])
        s_p, r, t = env.step(s, a)
        tau += 1
        tau[s, a] = 0

        Q[s, a] += alpha * (r + gamma * np.max(Q[s_p, :]) - Q[s, a])

        reward_model[s, a] = r
        s_p_model[s, a] = s_p

        for _ in range(n):
            s = np.random.choice(env.S)
            a = np.random.choice(env.A)
            r = reward_model[s, a]
            s_p = s_p_model[s, a]
            Q[s, a] += alpha * (
                r + k * sqrt(tau[s, a]) + gamma * np.max(Q[s_p]) - Q[s, a]
            )
            tau += 1
            tau[s, a] = 0

        s = s_p
        if env.is_terminal(s):
            s = env.reset()

    for s in env.S:
        if env.is_terminal(s):
            Pi[s, :] = 0.0
        Pi[s, :] = 0.0
        Pi[s, np.argmax(Q[s, :])] = 1.0

    return Pi, Q
