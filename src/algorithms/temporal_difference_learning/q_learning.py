from typing import Tuple

import numpy as np

from src import Env


def q_learning(
    env: Env,
    episodes: int = 10000,
    max_step_per_episode: int = 10,
    gamma: float = 0.99,
    alpha: float = 0.01,
    epsilon: float = 0.75,
) -> Tuple[np.ndarray, np.ndarray]:

    Pi = np.random.random((env.S.shape[0], env.A.shape[0]))
    Q = np.random.random((env.S.shape[0], env.A.shape[0]))

    for s in env.S:
        if env.is_terminal(s):
            Q[s, :] = 0.0
            Pi[s, :] = 0.0

    for _ in range(episodes):
        s = env.reset()
        step = 0

        while not env.is_terminal(s) and step < max_step_per_episode:
            rdm = np.random.random()
            a = np.random.choice(env.A) if rdm < epsilon else np.argmax(Q[s, :])
            s_p, r, t = env.step(s, a)
            Q[s, a] += alpha * (r + gamma * np.max(Q[s_p, :]) - Q[s, a])
            s = s_p
            step += 1

    for s in env.S:
        if env.is_terminal(s):
            Pi[s, :] = 0.0
        Pi[s, :] = 0.0
        Pi[s, np.argmax(Q[s, :])] = 1.0

    return Pi, Q
