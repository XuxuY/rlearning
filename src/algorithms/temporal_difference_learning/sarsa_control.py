from typing import Tuple

import numpy as np

from src import Env
from src.policies.tabular_random_uniform_policy import tabular_random_uniform_policy


def sarsa_control(
    env: Env,
    episodes: int = 10000,
    max_step_per_episode: int = 10,
    gamma: float = 0.99,
    alpha: float = 0.01,
    epsilon: float = 0.75,
) -> Tuple[np.ndarray, np.ndarray]:
    Pi = tabular_random_uniform_policy(env)
    Q = np.random.random((env.S.shape[0], env.A.shape[0]))

    for s in env.S:
        if env.is_terminal(s):
            Q[s, :] = 0.0
            Pi[s, :] = 0.0

    for _ in range(episodes):
        s = env.reset()
        step = 0
        rdm = np.random.random()

        if rdm < epsilon:
            a = np.random.choice(env.A)
        else:
            a = np.argmax(Q[s, :])

        while not env.is_terminal(s) and step < max_step_per_episode:
            s_p, r, t = env.step(s, a)
            rdm = np.random.random()
            if rdm < epsilon:
                a_p = np.random.choice(env.A)
            else:
                a_p = np.argmax(Q[s_p, :])
            Q[s, a] += alpha * (r + gamma * Q[s_p, a_p] - Q[s, a])
            s = s_p
            a = a_p
            step += 1

    for s in env.S:
        if env.is_terminal(s):
            Pi[s, :] = 0.0
        Pi[s, :] = epsilon / env.A.shape[0]
        Pi[s, np.argmax(Q[s, :])] = 1 - epsilon + epsilon / env.A.shape[0]

    return Pi, Q
