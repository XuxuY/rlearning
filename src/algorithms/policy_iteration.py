from typing import Tuple

import numpy as np

from src import Env
from src.algorithms.iterative_policy_evaluation import iterative_policy_evaluation


def policy_iteration(
    env: Env,
    P: np.ndarray,
    T: np.ndarray,
    Pi: np.ndarray,
    gamma: float = 0.99,
    theta: float = 1e-5,
) -> Tuple[np.ndarray, np.ndarray]:
    policy_stable = False
    while not policy_stable:
        V = iterative_policy_evaluation(env, P, T, Pi, gamma, theta)
        policy_stable = True
        for s in env.S:
            old_action = np.argmax(Pi[s])

            formula = lambda a: np.sum(  # noqa
                Pi[s, a] * P[s, a, s_p, 0] * (P[s, a, s_p, 1] + gamma * V[s_p])
                for s_p in env.S
            )
            best_action = max(env.A, key=formula)

            Pi[s] = 0.0
            Pi[s, best_action] = 1.0
            if old_action != best_action:
                policy_stable = False

    V = iterative_policy_evaluation(env, P, T, Pi, gamma, theta)
    return Pi, V
