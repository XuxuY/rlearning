from typing import Tuple

import numpy as np

from src import Env


def dunning_kruger(env: Env, gamma: float = 0.999) -> Tuple[np.ndarray, np.ndarray]:
    R = np.zeros((env.S.shape[0], env.A.shape[0]))

    temp_S = np.array(env.S, copy=True)
    np.random.shuffle(temp_S)

    for s in temp_S:
        if env.is_terminal(s):
            continue

        for a in env.A:
            s_p, r, _ = env.step(s, a)
            if r == 0:
                r += 1
            R[s, a] += r + (np.max(R[s_p]) * gamma) / r
    return R, R
