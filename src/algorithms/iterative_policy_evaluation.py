import numpy as np

from src import Env


def iterative_policy_evaluation(
    env: Env,
    P: np.ndarray,
    T: np.ndarray,
    Pi: np.ndarray,
    gamma: float = 0.99,
    theta: float = 1e-5,
) -> np.ndarray:
    V = np.random.random((env.S.shape[0],))
    V[T] = 0.0
    delta = float("inf")

    while delta > theta:
        delta = 0
        for s in env.S:
            v = V[s]
            V[s] = np.sum(
                Pi[s, a] * P[s, a, s_p, 0] * (P[s, a, s_p, 1] + gamma * V[s_p])
                for a in env.A
                for s_p in env.S
            )
            delta = np.max(np.array([delta, np.abs(v - V[s])]))
    return V
