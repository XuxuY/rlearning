from typing import Tuple

import numpy as np

from src import Env
from src.algorithms.monte_carlo.mc_es import generate_episode
from src.policies.tabular_random_uniform_policy import tabular_random_uniform_policy


def off_policy_mc_control(
    env: Env,
    episodes: int = 1000,
    max_step_per_episode: int = 100,
    gamma: float = 0.99,
    epsilon: float = 0.1,
    epsilon_greedy_behaviour_policy: bool = False,
) -> Tuple[np.ndarray, np.ndarray]:
    b = tabular_random_uniform_policy(env)
    Pi = tabular_random_uniform_policy(env)

    Q = np.random.random((env.S.shape[0], env.A.shape[0]))
    C = np.zeros((env.S.shape[0], env.A.shape[0]))

    for s in env.S:
        if env.is_terminal(s):
            Q[s, :] = 0.0
            Pi[s, :] = 0.0
        Pi[s, :] = 0
        Pi[s, np.argmax(Q[s, :])] = 1.0

    for _ in range(episodes):
        if epsilon_greedy_behaviour_policy:
            for s in env.S:
                b[s, :] = epsilon / env.A.shape[0]
                b[s, np.argmax(Q[s, :])] = 1.0 - epsilon + epsilon / env.A.shape[0]

        s0 = env.reset()

        s_l, a_l, _, r_l = generate_episode(s0, b, env, max_step_per_episode)

        G = 0.0
        W = 1
        for t in reversed(range(len(s_l))):
            st, at, rt = s_l[t], a_l[t], r_l[t]
            G = gamma * G + rt
            C[st, at] += W

            Q[st, at] += W / C[st, at] * (G - Q[st, at])
            Pi[st, :] = 0
            Pi[st, np.argmax(Q[st, :])] = 1.0
            if np.argmax(Q[st, :]) != at:
                break
            W = W / b[st, at]
    return Pi, Q
