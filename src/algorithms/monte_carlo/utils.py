from typing import List, Tuple

import numpy as np

from src import Env


def generate_episode(
    s0: int, Pi: np.ndarray, env: Env, max_step: int,
) -> Tuple[List[int], List[int], List[int], List[float]]:
    s_l, a_l, sp_l, r_l = [], [], [], []

    s = s0

    for _ in range(max_step):
        if env.is_terminal(s):
            break

        a: int = np.random.choice(env.A, p=Pi[s])
        s_p: int
        r: float
        s_p, r, _ = env.step(s, a)
        s_l.append(s)
        a_l.append(a)
        sp_l.append(s_p)
        r_l.append(r)
        s = s_p

    return s_l, a_l, sp_l, r_l
