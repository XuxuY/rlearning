from typing import Tuple

import numpy as np

from src import Env
from src.algorithms.monte_carlo.mc_es import generate_episode
from src.policies.tabular_random_uniform_policy import tabular_random_uniform_policy


def on_policy_first_visit_mc_control(
    env: Env,
    episodes: int = 1000,
    max_step_per_episode: int = 100,
    gamma: float = 0.99,
    epsilon: float = 0.1,
) -> Tuple[np.ndarray, np.ndarray]:
    Pi = tabular_random_uniform_policy(env)
    Q = np.random.random((env.S.shape[0], env.A.shape[0]))

    for s in env.S:
        if env.is_terminal(s):
            Q[s, :] = 0.0
            Pi[s, :] = 0.0

    returns = np.zeros((env.S.shape[0], env.A.shape[0]))
    returns_count = np.zeros((env.S.shape[0], env.A.shape[0]))

    for _ in range(episodes):
        s0 = np.random.choice(env.S)

        if env.is_terminal(s0):
            continue

        a0 = np.random.choice(env.A)

        s_p, r, terminal = env.step(s0, a0)

        s_l, a_l, _, r_l = generate_episode(s_p, Pi, env, max_step_per_episode)
        s_l.insert(0, s0)
        a_l.insert(0, a0)
        r_l.insert(0, r)

        G = 0.0
        for t in reversed(range(len(s_l))):
            st, at, rt = s_l[t], a_l[t], r_l[t]
            G = gamma * G + rt
            if (st, at) not in zip(s_l[:t], a_l[:t]):
                returns[st, at] += G
                returns_count[st, at] += 1
                Q[st, at] = returns[st, at] / returns_count[st, at]
                Pi[st, :] = epsilon / len(env.A)
                Pi[st, np.argmax(Q[st, :])] = 1 - epsilon + (epsilon / len(env.A))
    return Pi, Q
