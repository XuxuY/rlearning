from collections import Counter
from typing import Callable, List, Optional, Union

import numpy as np

from src.algorithms.dunning_kruger import dunning_kruger
from src.tictactoe import TicTacToe


class TicTacToeManager:
    PLAYER_BRAIN_INITIALIZER = Optional[Union[Callable, str]]
    PLAYER_BRAIN = Optional[np.ndarray]
    RANDOM = "random"

    def __init__(self) -> None:
        self.env = TicTacToe()
        self.current_player: int = 2

    def user_input(self) -> int:
        while True:
            try:
                move = int(
                    input(
                        f"It's your turn, {'X' if self.current_player == 1 else 'O'}. "
                        "Where will you play ? "
                    )
                )
                if not (0 < move < 10):
                    raise ValueError
                return move - 1
            except ValueError:
                print("Wrong choice !")

    def ask_move(self, board: np.ndarray, player_brain: PLAYER_BRAIN) -> int:
        if player_brain is None:
            while True:
                move = self.user_input()
                if board[move] == 0:
                    return move
        else:
            state = np.all(self.env._boards == board, axis=-1)
            state = np.where(state == True)[0][0]  # noqa
            q_state: np.ndarray = player_brain[state]

            for move in np.argsort(-q_state):
                if board[move] == 0:
                    return move
        raise Exception("Unreachable")

    def game(self, p1: PLAYER_BRAIN, p2: PLAYER_BRAIN, verbose: bool) -> int:
        self.current_player = 2
        board = np.zeros(9, dtype=np.int64)

        for i in range(board.shape[0]):
            self.current_player = self.current_player % 2 + 1

            if self.current_player == 1:
                if verbose and p1 is None:
                    self.env.display(board)
                move = self.ask_move(board, p1)
            else:
                if verbose and p2 is None:
                    self.env.display(board)
                move = self.ask_move(board, p2)
            if verbose:
                print(f"Player {self.current_player} played : {move + 1}")

            board[move] = self.current_player

            # Now we will check if player X or O has won,for every move after 5 moves.
            if self.env.is_terminal_board(board):
                if verbose:
                    self.env.display(board)
                    print(f"Game set : Player {self.current_player} win !")
                return self.current_player

        # If neither X nor O wins and the board is full,
        # we'll declare the result as 'tie'.
        if verbose:
            print("\nGame Over.\n")
            print("It's a Tie!!")
        return 0

    def meta_game(
        self,
        p1: PLAYER_BRAIN_INITIALIZER = None,
        p2: PLAYER_BRAIN_INITIALIZER = None,
        max_game: int = 1000,
        verbose: bool = True,
    ) -> List[int]:
        if p1 is None or p2 is None:
            verbose = True

        p1_choice = p1
        p2_choice = p2

        print("Players are preparing...")
        if p1_choice == "random":
            p1 = self._gen_Q()
        elif p1:
            _, p1 = p1(self.env)
        if p2_choice == "random":
            p2 = self._gen_Q()
        elif p2:
            _, p2 = p2(self.env)
        print("Players are ready !")

        game_results = []
        for _ in range(max_game):
            game_result = self.game(p1, p2, verbose)
            game_results.append(game_result)

            if p1 is None or p2 is None:
                if input("Do want to play Again? (y/n)").lower() == "n":
                    break
            if p1_choice == "random":
                p1 = self._gen_Q()
            if p2_choice == "random":
                p2 = self._gen_Q()

        return game_results

    def _gen_Q(self) -> np.ndarray:
        return np.random.random((self.env.S.shape[0], self.env.A.shape[0]))


if __name__ == "__main__":
    tic_tac_toe = TicTacToeManager()
    results = tic_tac_toe.meta_game(
        p1=dunning_kruger, p2=TicTacToeManager.RANDOM, max_game=1000, verbose=False
    )
    print(Counter(results))
