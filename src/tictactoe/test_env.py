from src.algorithms.monte_carlo.mc_es import mc_es
from src.tictactoe import TicTacToe
from src.utils import env_tester

if __name__ == "__main__":
    extra_args = dict(
        expected_sarsa=dict(episodes=10000, max_step_per_episode=100),
        mc_es=dict(episodes=10),
    )

    env = TicTacToe()
    env_tester(
        env,
        extra_args=extra_args,
        # Q_Pi_apply=env.pretty_print_sample,
        algorithms=(mc_es,),
    )
