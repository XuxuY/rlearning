from itertools import product
from typing import Iterator, Optional, Set, Tuple

import numpy as np

from src import Env


class TicTacToe(Env):
    def __init__(self, width: int = 3):
        all_s = np.array(list(product([0, 1, 2], repeat=width ** 2)))
        self.width = width
        self._boards = all_s
        self._S = np.arange(self._boards.shape[0])
        self._A = np.arange(width ** 2)

    @property
    def A(self) -> np.ndarray:
        return self._A

    @property
    def S(self) -> np.ndarray:
        return self._S

    def step(self, state: int, action: int) -> Tuple[int, float, bool]:
        new_board = np.array(self._boards[state], copy=True)
        player = TicTacToe._get_player(new_board)
        new_board[action] = player

        next_state = np.all(self._boards == new_board, axis=-1)
        next_state = np.where(next_state == True)[0][0]  # noqa
        is_terminal = self.is_terminal_board(new_board)
        reward = self._get_reward(state, next_state, action, is_terminal, player)
        return next_state, reward, is_terminal

    def is_terminal(self, state: int) -> bool:
        board = self._boards[state]
        return not TicTacToe._board_is_valid(board) or self.is_terminal_board(board)

    def is_terminal_board(self, board: np.ndarray) -> bool:
        return any(self._is_win(line) for line, _ in self.board_lines(board))

    def board_lines(self, board: np.array) -> Iterator[Tuple[np.ndarray, np.ndarray]]:
        w = self.width
        board = board.reshape((w, w))

        # Rows
        for i in range(w):
            index = np.arange(w) + np.full(w, fill_value=i * w)
            yield board[i, :], index

        # Cols
        for i in range(w):
            index = np.arange(w ** 2, step=w) + i
            yield board[:, i], index

        # Diag
        index = np.arange(w ** 2, step=w + 1)
        yield np.diag(board), index
        # Opposite diag
        index = np.arange(start=w - 1, stop=w ** 2 - 1, step=w - 1)
        yield np.diag(np.fliplr(board)), index

    @staticmethod
    def _is_win(line: np.ndarray) -> bool:
        uniques = np.unique(line)
        return uniques.shape[0] == 1 and uniques[0] != 0

    @staticmethod
    def _is_almost_win(board: np.ndarray, player: int) -> bool:
        uniques, counts = np.unique(board, return_counts=True)

        # The two player are already on the line
        if (1 in uniques and 2 in uniques) or (uniques.shape[0] == 1):
            return False

        # There must be at least a zero
        return counts[0] == 1 and uniques[1] == player

    def positions_to_win(self, state: int, player: int) -> Set[int]:
        board = self._boards[state]
        p = set()
        for line, indexes in self.board_lines(board):
            if TicTacToe._is_almost_win(line, player):
                index = np.where(line == 0)[0][0]
                p.add(indexes[index])
        return p

    def positions_to_counter(self, state: int, player: int) -> Set[int]:
        return self.positions_to_win(state, player % 2 + 1)

    def _get_reward(
        self,
        old_state: int,
        next_state: int,
        action: int,
        is_terminal: bool,
        player: int,
    ) -> float:
        if not TicTacToe._board_is_valid(self._boards[next_state]):
            return 0

        if self._is_wrong_choice(old_state, action):
            return -200

        if is_terminal:
            return 100

        ptw = self.positions_to_win(old_state, player)
        ptc = self.positions_to_counter(old_state, player)
        if ptw:
            return 50 * (1 if action in ptw else -1)
        elif ptc:
            return 25 * (1 if action in ptc else -1)
        else:
            return 0

    def reset(self) -> int:
        return 0

    def _is_wrong_choice(self, state: int, action: int) -> bool:
        return self._boards[state][action] != 0

    @staticmethod
    def _board_is_valid(board: np.ndarray) -> bool:
        return 0 <= TicTacToe._get_player(board) - 1 <= 1

    @staticmethod
    def _get_player(board: np.ndarray) -> int:
        uniques, counts = np.unique(board, return_counts=True)
        d = dict(zip(uniques, counts))
        return d.get(1, 0) - d.get(2, 0) + 1

    def display(self, board: np.ndarray) -> None:
        board = board.reshape((self.width, self.width))
        sd = {0: "", 1: "X", 2: "O", 3: "☭"}
        sl = []
        for row in board:
            sl.append("│".join(sd[x].center(3) for x in row))
            sl.append("───┼───┼───")
        del sl[-1]
        print("\n".join(sl))

    def pretty_print_sample(
        self, Q: np.ndarray, _Pi: Optional[np.ndarray] = None, sample_size: int = 30
    ):
        Q_sample_indexes = np.random.choice(
            np.arange(Q.shape[0]), size=sample_size, replace=False
        )
        for q_i in Q_sample_indexes:
            board = np.array(self._boards[q_i], copy=True)
            q = Q[q_i]
            board[np.argmax(q)] = 3
            self.display(board)
            print()
