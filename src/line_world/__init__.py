from typing import Optional, Tuple

import numpy as np

from src import Env


class LineWorld(Env):
    """
    S --> States : Possibles states at each step.
    A --> Actions : Possibles actions at each step.
    T --> Terminal states : States where the simulation ends.
    P --> Probabilities (with Reward) :
              State-Action-NextState-(Probability, Reward) matrix.
    """

    NUM_ACTIONS = 2

    def __init__(self, num_states: int = 5) -> None:
        self.num_states = num_states
        self._S = np.arange(num_states)
        self._A = np.arange(LineWorld.NUM_ACTIONS)
        self.T = np.array([0, self.num_states - 1])
        self.P = self._init_P()

    @property
    def A(self) -> np.ndarray:
        return self._A

    @property
    def S(self) -> np.ndarray:
        return self._S

    def _init_P(self) -> np.ndarray:
        P = np.zeros((len(self.S), len(self.S), len(self.S), 2))
        for s in self.S[1:-1]:
            # En faisant l'action A à partir de l'état S,
            # il y a une probabilité de 1.0 d'arriver à l'état S-1.
            P[s, 0, s - 1, 0] = 1.0
            P[s, 1, s + 1, 0] = 1.0

        # À partir de l'état 1 (l'avant dernier plus à gauche),
        # aller à gauche octroit un Reward R de -1.0
        P[1, 0, 0, 1] = -1.0
        # À partir de l'état num_states - 2 (l'avant dernier plus à droite),
        # aller à droite octroit un Reward R de 1.0
        P[self.num_states - 2, 1, self.num_states - 1, 1] = 1.0

        # Tous les autres Reward R sont à 0.0 (car P a été crée avec np.zeros)
        return P

    def reset(self) -> int:
        return LineWorld.NUM_ACTIONS // 2

    def is_terminal(self, state: int) -> bool:
        return state in self.T

    def step(self, state: int, action: int) -> Tuple[int, float, bool]:
        assert not self.is_terminal(state)

        # Avec S et A, on obtient S' avec pour p chaque p pour chaque S'
        next_state = np.random.choice(self.S, p=self.P[state, action, :, 0])

        # On récupère R pour la combinaison S, A, S', 1 (index pour R)
        reward = self.P[state, action, next_state, 1]
        return next_state, reward, self.is_terminal(next_state)

    def pretty_print(self, Q: np.ndarray, _: Optional[np.ndarray] = None) -> None:
        d = {0: "←", 1: "→"}

        arrows = np.array([d[np.argmax(q)] for q in Q])
        arrows[self.T] = "★"
        print(str(arrows).replace("'", " "))
