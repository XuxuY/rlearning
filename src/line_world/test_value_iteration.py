from time import time

from src.algorithms.value_iteration import value_iteration
from src.line_world import LineWorld
from src.policies.tabular_random_uniform_policy import tabular_random_uniform_policy

if __name__ == "__main__":
    st = time()

    env = LineWorld()
    Pi = tabular_random_uniform_policy(env)

    V, Pi = value_iteration(env, env.P, env.T, Pi)

    print(V)
    env.pretty_print(Pi)

    print(f"Time to execute : {time() - st}s")
