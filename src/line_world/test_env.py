from src.line_world import LineWorld
from src.utils import env_tester

if __name__ == "__main__":
    extra_args = dict(expected_sarsa=dict(episodes=10000, max_step_per_episode=100))

    env = LineWorld()
    env_tester(env, extra_args=extra_args, Q_Pi_apply=env.pretty_print)
