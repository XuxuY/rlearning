from abc import ABCMeta, abstractmethod
from typing import Tuple

import numpy as np


class Env(metaclass=ABCMeta):
    @property
    @abstractmethod
    def A(self) -> np.ndarray:
        pass

    @property
    @abstractmethod
    def S(self) -> np.ndarray:
        pass

    @abstractmethod
    def step(self, state: int, action: int) -> Tuple[int, float, bool]:
        """
        :return: next_state, reward, is_terminal value
        """
        pass

    @abstractmethod
    def reset(self) -> int:
        pass

    @abstractmethod
    def is_terminal(self, state: int) -> bool:
        pass
