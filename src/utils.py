from copy import deepcopy
from time import time
from typing import Callable, Dict, Iterable, Optional, Tuple, Union

import numpy as np

from src import Env
from src.algorithms.monte_carlo.mc_es import mc_es
from src.algorithms.monte_carlo.off_policy_mc_control import off_policy_mc_control
from src.algorithms.monte_carlo.on_policy_first_visit_mc_control import (
    on_policy_first_visit_mc_control,
)
from src.algorithms.planning.dyna_q import dyna_q
from src.algorithms.planning.dyna_q_plus import dyna_q_plus
from src.algorithms.temporal_difference_learning.expected_sarsa import expected_sarsa
from src.algorithms.temporal_difference_learning.q_learning import q_learning
from src.algorithms.temporal_difference_learning.sarsa_control import sarsa_control

ALGORITHMS = (
    mc_es,
    off_policy_mc_control,
    on_policy_first_visit_mc_control,
    dyna_q,
    dyna_q_plus,
    expected_sarsa,
    q_learning,
    sarsa_control,
)

ALGORITHMS_KWARGS = Dict[str, Union[int, float]]


def env_tester(
    env: Env,
    extra_args: Optional[Dict[str, ALGORITHMS_KWARGS]] = None,
    Q_Pi_apply: Optional[Callable] = None,
    algorithms: Optional[Iterable[Callable]] = None,
) -> None:
    algorithms = algorithms or ALGORITHMS
    extra_args = extra_args or {}

    for algorithm in algorithms:
        algorithm_name = algorithm.__name__.replace("_", " ").title()
        a_extra_args = extra_args.get(algorithm.__name__, {})

        if a_extra_args.get("no_run", False):
            continue
        a_extra_args.pop("no_run", None)

        Q, Pi, execution_time = _test_algorithm(algorithm, env, **a_extra_args)
        execution_time = round(execution_time, 3)

        print(f"{algorithm_name} : {execution_time}s")
        if Q_Pi_apply:
            Q_Pi_apply(Q, Pi)
            print()


def _test_algorithm(
    algorithm: Callable, env: Env, **kwargs
) -> Tuple[np.ndarray, np.ndarray, float]:
    st = time()
    Pi, Q = algorithm(deepcopy(env), **kwargs)
    return Pi, Q, time() - st
