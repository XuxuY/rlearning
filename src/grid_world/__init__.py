from typing import Optional, Tuple

import numpy as np

from src import Env


class GridWorld(Env):
    NUM_ACTIONS = 4  # 0: left, 1: Right, 2: Up, 3: Down

    def __init__(self, height: int = 4, width: int = 4) -> None:
        self.height = height
        self.width = width
        self.num_states = width * height
        self._S = np.arange(self.num_states)
        self._A = np.arange(GridWorld.NUM_ACTIONS)
        self.T = np.array([self.width - 1, self.num_states - 1])
        self.P = self._init_P()

    @property
    def A(self) -> np.ndarray:
        return self._A

    @property
    def S(self) -> np.ndarray:
        return self._S

    def _init_P(self) -> np.ndarray:
        P = np.zeros((len(self.S), len(self.A), len(self.S), 2))

        for s in self.S:
            # Si on est sur la première colonne, on a une probabilité de 1
            # d'arriver sur la même case en allant à gauche (A = 0)
            if (s % self.width) == 0:
                P[s, 0, s, 0] = 1.0
            # Pour le reste, p = 1 d'arriver sur s - 1
            else:
                P[s, 0, s - 1, 0] = 1.0

            # So on est sur la dernière colonne, on a une probabilité de 1
            # d'arriver sur la même case en allant à droite (A = 1)
            if (s + 1) % self.width == 0:
                P[s, 1, s, 0] = 1.0
            # Pour le reste, p = 1 d'arriver sur s + 1
            else:
                P[s, 1, s + 1, 0] = 1.0

            # Pour la ligne du haut, on a une probabilté de 1
            # d'arriver sur la même case en montant (A = 2)
            if s < self.width:
                P[s, 2, s, 0] = 1.0
            # Pour le reste, p = 1 d'arriver sur s - width
            else:
                P[s, 2, s - self.width, 0] = 1.0

            # Pour la ligne du bas, on a une probabilité de 1
            # d'arriver sur la même case en descendant (A = 3)
            if s >= (self.num_states - self.width):
                P[s, 3, s, 0] = 1.0
            # Pour le reste, p = 1 d'arriver sur s + width
            else:
                P[s, 3, s + self.width, 0] = 1.0

        # Pour les états terminaux, la probabilité de faire une action est de 0
        P[self.width - 1, :, :, 0] = 0.0
        P[self.num_states - 1, :, :, 0] = 0.0

        # Angle haut-droite : reward = -5.0
        # Angle bas-droite : reward = 1.0
        # Pour le reste : reward = 0.0
        P[:, :, self.width - 1, 1] = -5.0
        P[:, :, self.num_states - 1, 1] = 1.0
        return P

    def reset(self) -> int:
        return 0

    def is_terminal(self, state: int) -> bool:
        return state in self.T

    def step(self, state: int, action: int) -> Tuple[int, float, bool]:
        assert not self.is_terminal(state)
        next_state = np.random.choice(self.S, p=self.P[state, action, :, 0])
        reward = self.P[state, action, next_state, 1]
        return next_state, reward, self.is_terminal(state)

    def pretty_print(self, Q: np.ndarray, _: Optional[np.ndarray] = None) -> None:
        d = {0: "←", 1: "→", 2: "↑", 3: "↓"}

        arrows = np.array([d[np.argmax(q)] for q in Q])
        arrows[self.T] = "★"
        print(str(arrows.reshape((self.width, self.height))).replace("'", " "))
