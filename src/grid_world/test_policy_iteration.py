from time import time

from src.algorithms.policy_iteration import policy_iteration
from src.grid_world import GridWorld
from src.policies.tabular_random_uniform_policy import tabular_random_uniform_policy

if __name__ == "__main__":
    st = time()

    env = GridWorld()
    Pi = tabular_random_uniform_policy(env)

    Pi, V = policy_iteration(env, env.P, env.T, Pi)
    print(V.reshape((env.width, env.height)))
    env.pretty_print(Pi)

    print(f"Time to execute : {time() - st}s")
